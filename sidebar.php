<div id="sidebar">
    <div class="sidebar-box">
        <h6 class="sidebar-h6" id="sbh6">Recent Posts</h6>
        <ul style="padding: 0 10px; list-style: none;">
            <?php $recPosts = new WP_Query( array( 'posts_per_page' => 4 ) ); ?>
            <?php if($recPosts->have_posts()): ?>
                <?php while($recPosts->have_posts()): $recPosts->the_post(); ?>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
        </ul>
    </div>
    <div class="sidebar-box">
        <h6>Categories</h6>
            <ul style="padding: 0 10px; list-style: none;">
                <?php
                    wp_list_categories(
                        array(
                            'orderby' => 'name',
                            'title_li' => ''
                            ));?>
            </ul>
    </div>
    <div style="margin-bottom: 20px;">
        <h6 class="sidebar-h6">Archives</h6>
        <select class="form-control" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;" >
            <option selected="selected" value="">Select Month</option>
            <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
        </select>
    </div>
</div>

