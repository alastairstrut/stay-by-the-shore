<?php get_header();?>

<div class="text-drawer">
    <div class="container" style="max-width: none;">
        <div class="row">
            <div class="col-12">
                <?php if(have_posts()):
                    while(have_posts()): the_post();
                        the_content();
                    endwhile;
                endif;?>
            </div>
        </div>
    </div>
</div>

<div class="info-ctas">
    <div class="container">
        <div class="row">
            <div class="col-xl-3">
                <div class="box-1">
                    <img class="box-1-svg" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/birds-svg.svg" alt="birds">
                    <h3>The ideal base to explore Wester Ross & beyond</h3>
                    <p>We are an ideal base on the North Coast 500 to explore the remote yet breathtaking scenery of Wester Ross. To the south is the Applecross peninsula and Torridon range and to the north Gruinard Bay and Ullapool all within a 1 to 2 hours drive of Gairloch.</p>
                    <a class="btn button-a" href="<?php bloginfo('url'); ?>/the-cottage/">The Cottage</a>
                </div>
            </div>
            <div id="cta-image-1" class="col-xl-9" style="background-position: bottom;">
            </div>
        </div>
        <div class="row" id="row-margin">
            <div id="cta-image-2" class="col-xl-9 order-2">
            </div>
            <div class="col-xl-3 order-1 order-xl-12">
                <div class="box-2">
                    <h3>Captivating Scenery & Wildlife</h3>
                    <p>The famous beach at Red Point (featured in the film ‘What we did on our Holiday’ starring Billy Connolly) and Big Sands two miles away are perfect locations to watch the amazing sunsets with the Isle of Skye in the far distance. There are also several whale, dolphin and wildlife trips operating from Gairloch Harbour along with endless opportunities to walk in the surrounding hills.</p>
                    <a class="btn button-a" href="<?php bloginfo('url'); ?>/see-do/">See & Do</a>
                    <img class="box-2-svg" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/waves-svg.svg" alt="waves">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="book-cta">
    <div class="book-box">
        <h2>Stay with us</h2>
        <p>Whether you stay one night or one week with us, we pride ourselves on going the extra mile to try and ensure our guests have a relaxing and memorable stay – we want you to fall in love with Gairloch and Wester Ross as much as we have.</p>
        <a class="btn button-a" href="<?php bloginfo('url'); ?>/booking/">Book Now</a>
    </div>
</div>

<div class="news-drawer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12" style="text-align: center;">
                <h2>Latest News</h2>
            </div>
        </div>

        <?php $args = array(
            'type' => 'post',
            'posts_per_page' => 1,);
        $args2 = array(
            'type' => 'post',
            'posts_per_page' => 1,
            'offset' => 1);
        $lastBlog = new wp_query($args);
        $SecondlastBlog = new wp_query($args2);?>
            <div class="row" style="text-align: left;">
                <?php if($lastBlog->have_posts()):
                    while($lastBlog->have_posts()): $lastBlog->the_post(); ?>

<?php $blogImg = "";

if (has_post_thumbnail($piD)) {
    $blogfeatImg = wp_get_attachment_image_url( get_post_thumbnail_id( ), 'large');
	if($blogfeatImg){
		$blogImg = $blogfeatImg;
	}
}?>

    <div class="col-lg-3" id="news-image-1" <?php if(!empty($blogImg)) echo 'style="background-image: url(' . $blogImg  . ')"'; ?>>
    </div>
    <div class="col-lg-4 news-box-1">
        <p><?php the_date("d/m/y"); ?></p>
        <h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
    </div>
                <?php endwhile;
            endif; wp_reset_postdata(); ?>
            <?php if($SecondlastBlog->have_posts()):
                while($SecondlastBlog->have_posts()): $SecondlastBlog->the_post(); ?>

<?php $blogImg = "";

if (has_post_thumbnail($piD)) {
	$blogfeatImg = wp_get_attachment_image_url( get_post_thumbnail_id( ), 'large');
	if($blogfeatImg){
		$blogImg = $blogfeatImg;
	}
}?>

    <div class="col-lg-5" id="nb2p">
        <div id="news-image-2" <?php if(!empty($blogImg)) echo 'style="background-image: url(' . $blogImg  . ')"'; ?>>
        </div>
        <div class="news-box-2">
            <p><?php the_date("d/m/y"); ?></p>
            <h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
        </div>
    </div>
            <?php endwhile;
        endif; wp_reset_postdata(); ?>
</div>
    <div class="row">
        <a class="btn button-a" href="<?php bloginfo('url'); ?>/blog">View Our Blog</a>
    </div>
    </div>
</div>
<?php get_footer();?>