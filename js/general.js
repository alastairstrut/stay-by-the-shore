//javascript functions

document.getElementById("hamburger").addEventListener("click", menutoggle);

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function menutoggle(){
    var x = document.getElementById("fsmenu");
    // Gets rid of the Menu
    if (x.style.height === "100%") {
        x.style.height = "0%";
        x.style.transitionDelay = ".5s";
        x.style.transitionDuration = ".5s";

        jQuery('.fsul')
        .fadeOut(500)

        jQuery('.header-nav-2')
        .fadeOut(500)

        document.getElementById("hbcheckbox").checked = false;
            if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
            document.removeEventListener('wheel', preventDefault, {passive: false}); // Enable scrolling in Chrome
            window.onmousewheel = document.onmousewheel = null;
            window.onwheel = null;
            window.ontouchmove = null;
            document.onkeydown = null;
            document.documentElement.style.overflow = 'auto';  // firefox, chrome
            document.body.scroll = "yes"; // ie only
    } else {
        // Drops Down Menu
        x.style.height = "100%";
        x.style.transitionDelay = "0s";
        x.style.transitionDuration = "1s";

		jQuery('.fsul')
        .delay(500)
        .fadeIn(500)

        jQuery('.header-nav-2')
        .delay(500)
        .fadeIn(500)

        document.getElementById("hbcheckbox").checked = true;
            if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
            document.addEventListener('wheel', preventDefault, {passive: false}); // Disable scrolling in Chrome
            window.onwheel = preventDefault; // modern standard
            window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
            window.ontouchmove = preventDefault; // mobile
            document.onkeydown = preventDefaultForScrollKeys;
            document.documentElement.style.overflow = 'hidden';  // firefox, chrome
            document.body.scroll = "no"; // ie only
    }
}

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
        e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }