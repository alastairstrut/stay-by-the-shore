/**
 * Modern lazy load using IntersectionObserver
 * -------------------------------------------
 *
 * Forked from: https://codepen.io/mishunov/pen/qpmWYP
 * Based on content from smashing magazine article: https://www.smashingmagazine.com/2018/01/deferring-lazy-loading-intersection-observer-api/
 * Uses IntersectionObserver pollyfill: https://raw.githubusercontent.com/w3c/IntersectionObserver/master/polyfill/intersection-observer.js
 *
 *
 *
 *
 *
 * Instructions for use:
 * --------------------
 *
 * - Image tags:
 *   ----------
 *       <img src="x.jpg">
 *           change to
 *       <img data-src="x.jpg" src="placeholder.png">
 *
 * - Div Background Images:
 *   ---------------------
 *       <div style="background-image: url(x.jpg)">...</div>
 *           change to
 *       <div data-bg-img="x.jpg" style="background-image: url(placeholder.png)">...</div>
 *
 */

var StrutLazeeeLoad = {
    config: {
      rootMargin: '0px 0px 50px 0px',
      threshold: 0.1
    },

    init: function(){
        var _self = this;
        var observer = new IntersectionObserver(function (entries, self) {

            if(entries){
                for(var i = 0; i < entries.length; i++){
                    var entry = entries[i];
                    if (entry.isIntersecting) {

                        _self.lazyLoadImg(entry.target);

                        // Stop watching and load the image
                        self.unobserve(entry.target);
                    }
                }
            }
        }, _self.config);

        var images = document.querySelectorAll('img[data-src],[data-bg-img]');
        if(images){
            for(var i = 0; i < images.length; i++){
                var image = images[i];
                observer.observe(image);
            }
        }

        return _self;

    },

    lazyLoadImg: function(img) {
        var src = false;

        if(img.getAttribute('data-src') !== null){

            src = img.getAttribute('data-src');

            if(src !== img.getAttribute('src')){
                imgO = new Image();
                imgO.src = src;
                imgO.addEventListener('load', function(){
                    img.src = src;
                    img.removeAttribute('data-src');
                    // img.style.opacity = 1;
                });
            }

        }else if(img.getAttribute('data-bg-img') != null){

            if(src !== img.style.backgroundImage){
                imgO = new Image();
                imgO.src = img.getAttribute('data-bg-img')
                imgO.addEventListener('load', function(){
                    img.style.backgroundImage = 'url("'+ img.getAttribute('data-bg-img') + '")';
                    img.removeAttribute('data-bg-img');
                    // img.style.opacity = 1;
                });
            }
        }
    }
};

var strutLazeeeLoad;

(function(){
    var strutLazeeeLoad = StrutLazeeeLoad.init();

})();

