<?php get_header();?>
    <div class="text-drawer">
        <div class="container" style="max-width: none;">
            <div class="row">
                <div class="col-12">
                <article>
                        <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post(); ?>
                                <h2><?php the_title(); ?></h2>
                                <div><?php the_content(); ?></div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </article>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>