<?php get_header();?>
<div class="text-drawer">
    <div class="container" style="max-width: none;">
        <div class="row">
            <div class="col-12">
                <h2 style="margin-bottom: 50px; text-align: center;">Archive for <?php the_time('F, Y'); ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9" id="after-sidebar">
                <article>
                    <?php if( have_posts() ):
                        while( have_posts() ): the_post(); ?>
                            <?php get_template_part('includes/blogpost' ); ?>
                        <?php endwhile; ?>
                        <div class="row">
                            <div class="col-6 text-left" style="padding: 10px 0 10px;">
                                <?php next_posts_link('< Older Posts'); ?>
                            </div>
                            <div class="col-6 text-right" style="padding: 10px 0 10px;">
                                <?php previous_posts_link('Newer Posts >'); ?>
                            </div>
                        </div>
                    <?php endif;?>
                </article>
            </div>
            <div class="col-lg-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>