<?php 

/*
Template Name: Map Template
*/

get_header();?>
    <div class="text-drawer">
        <div class="container" style="max-width: none;">
            <div class="row">
                <div class="col-12">
                <article>
                        <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post(); ?>
                                <h2><?php the_title(); ?></h2>
                                <div><?php the_content(); ?></div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
                        <div class="row">
                            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2130.1043151672106!2d-5.706504483854285!3d57.73167084511285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x488e7158a4c7985b%3A0xd296d9874bb9b3a9!2sOld%20School%20House%20B%26B!5e0!3m2!1sen!2suk!4v1579706216230!5m2!1sen!2suk" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                    </div>
<?php get_footer();?>