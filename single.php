<?php get_header();?>
    <div class="text-drawer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <article>
                        <?php
                        if(have_posts()):
                            while(have_posts()): the_post(); ?>
                            <h2><?php the_title(); ?></h2>
                            <p><?php edit_post_link(); ?></p>
                            <p><?php the_time('g:i a'); ?> on <?php the_time('d/m/y'); ?></p>
                            <?php the_content(); ?>
                            <?php endwhile;
                        endif;?>
                    </article>
                    <a href="<?php bloginfo('url'); ?>/blog/" class="btn button-a" style="margin-top: 30px;">Back to Blog</a>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>