<?php
    global $post;
    $pid = $post->ID;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php wp_title(); ?></title>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.min.css">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php
//Try and fall back on featured image

$headerImg = "";

if (has_post_thumbnail($piD)) {
	$featImg = wp_get_attachment_image_url(get_post_thumbnail_id($piD), $size = 'header-size');
	if($featImg){
		$headerImg = $featImg;
	}
}?>

    <header <?php if(!empty($headerImg)) echo 'style="background-image: url(' . $headerImg  . ')"'; ?>>
    <div style="height: 100%; width: 100%; background-color: #00000026;">

        <div id="fsmenu">
        <div class="header-nav-2">
            <a href="tel:+4401445712814">
                <img id="h-phone-icon-2" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/header-phone-icon.svg" alt="phone">
            </a>
            <a href="mailto:staybytheshore@gmail.com">
                <img id="h-mail-icon-2" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/header-mail-icon.svg" alt="mail">
            </a>
        </div>
        <div class="fsul">
        <ul>
            <?php wp_list_pages('depth=2&title_li='); ?>
        </ul>
    </div>
    </div>

    <div class="header-nav">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 d-none d-md-block" style="text-align: left;">
                        <a class="btn button-b" href="<?php bloginfo('url'); ?>/booking/">Book Now</a>
                        <a href="tel:+4401445712814">
                            <img id="h-phone-icon" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/header-phone-icon.svg" alt="phone">
                        </a>
                        <a href="mailto:staybytheshore@gmail.com">
                            <img id="h-mail-icon" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/header-mail-icon.svg" alt="mail">
                        </a>
                    </div>
                    <div class="col-4 d-md-none" style="text-align: left;">
                        <a class="btn button-b" href="<?php bloginfo('url'); ?>/booking/">Book</a>
                    </div>
                    <div class="col-4" style="text-align: center;">
                        <a href="<?php bloginfo('template_url'); ?>/home/">
                            <img id="header-logo" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/nav-logo.svg" alt="logo">
                        </a>
                    </div>
                    <div class="col-4" style="text-align: right;">
                        <input id="hbcheckbox" type="checkbox">
                        <a href="tel:+4401445712814">
                            <img id="h-phone-icon-3" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/header-phone-icon.svg" alt="phone">
                        </a>
                        <a id="hamburger">
                            <div id="a"></div>
                            <div id="b"></div>
                            <div id="c"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </header>