<?php get_header();?>
    <div class="text-drawer">
        <div class="container">
            <div class="row" style="display: flow-root;">
                <div class="col-12">
                    <article>
                        <h2>Page not found</h2>
                        <p>Sorry, the page you were looking for could not be found. Please try using the main navigation at the top to find what you are looking for.</p>
                    </article>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>