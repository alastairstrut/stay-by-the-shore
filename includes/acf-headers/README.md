Installation
============
In your functions file near the top add the following line:

get_template_directory() . '/includes/acf-headers/index.php';

Install and activate ACF, then import the headers.json file to setup the admin


Example Markup
--------------

See example-php-markup folder for a general implementation guide