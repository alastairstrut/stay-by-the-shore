var slideshow_speed = 5 * 1000; //num seconds * 1000 to convert to miliseconds;

/** DO NOT EDIT BELOW HERE **/
var currentSlide=0;
var currentExtraHeader = 0;

jQuery(window).load(function(){
    if(typeof extraHeaders !== "undefined" && extraHeaders){
        loadExtraHeader();
    }
});



function loadExtraHeader(){
    jQuery('#hs-headers').append('<div id="loadheader' + currentExtraHeader + '" class="headerimg"><!-- EMPTY --></div>');
    var imgtmp = new Image();
    imgtmp.onload = function(){
        jQuery('#loadheader' + currentExtraHeader).css({
            'display': 'none',
            'background-image':'url(\''+extraHeaders[currentExtraHeader]+'\')'
        });
        currentExtraHeader++;
        if(currentExtraHeader<extraHeaders.length){
            loadExtraHeader();
        }else{
            window.setInterval(runSlideNew,slideshow_speed);
        }
    };
    imgtmp.src = extraHeaders[currentExtraHeader];
}

function runSlideNew(){

    slideRunning = false;
    var nextImage = currentSlide + 1;
    if(nextImage>=jQuery('#hs-headers .headerimg').size()){
        nextImage = 0;
    }

    jQuery('#hs-headers .headerimg:eq('+nextImage+')').css("z-index", 1).show();
    jQuery('#hs-headers .headerimg:eq('+currentSlide+')').css("z-index", 100).delay(1000).fadeOut(2000,function(){
            jQuery('#hs-headers .headerimg:eq('+currentSlide+')').css("z-index", 1);
            jQuery('#hs-headers .headerimg:eq('+nextImage+')').css("z-index", 100);
            slideRunning = true;
            currentSlide++;
            if(currentSlide>=jQuery('#hs-headers .headerimg').size()){
                currentSlide = 0;
            }
    });
}
