<header>
    <div id="hs-headers">

        <?php
            //Default to nothing
            $title = $subTitle = '';

            //Check for 404 edge case
            if(is_404()){
                $title = 'Page Not Found';
            }else{
                // page title used by default
                $title = get_the_title($pid);

                //Check for ACF override
                if(get_field('title', $pid) && !empty(trim(get_field('title', $pid)))){
                    $title = trim(get_field('title', $pid));
                }

                if(get_field('secondary_title', $pid) && !empty(trim(get_field('secondary_title', $pid)))){
                    $subTitle = trim(get_field('secondary_title', $pid));
                }
            }
        ?>
        <div id="header-content">
            <h1><?php echo $title; ?></h1>
            <?php if($subTitle !== ''): ?>
                <hr>
                <h5><?php echo $subTitle; ?></h5>
            <?php endif; ?>
        </div>

        <?php
            //Setup vars
            $imgPool = $type = $acfImgPool = $headerImgID = $headerImgSrc = $headerImgSrcSet = null;

            //Check for images set
            if(get_field('image_pool', $pid) && !empty(get_field('image_pool', $pid))){

                $acfImgPool = get_field('image_pool', $pid);
                if($acfImgPool && is_array($acfImgPool)){

                    foreach($acfImgPool as $headerImgID){

                        $headerImgSrcSet = wp_get_attachment_image_srcset($headerImgID);
                        $headerImgSrc = wp_get_attachment_image_src( $headerImgID, 'origonal');
                        $headerImgSrc = $headerImgSrc[0];

                        $imgPool[] = array(
                            'ID' => $headerImgID,
                            'src' => $headerImgSrc,
                            'srcset' => $headerImgSrcSet
                        );
                    }
                }

            }else{ //no images, setup img pool with default header
                $imgPool = array(array('src' => get_bloginfo('template_url') . '/assets/header.jpg'));
            }


            /**
             * Switch slideshow type:
             * slideshow : Regular slideshow in order set in admin
             * slideshow-random : Randomize the order of the imgPool
             * single: Img pool has only one img in it
             * single-random : Pick one item of the imgPool at random
             */
            $type = get_field('type', $pid);
            switch($type){
                case 'slideshow-random': shuffle($imgPool); break;
                case 'single-random': $imgPool = array($imgPool[array_rand($imgPool)]); break;
            }
        ?>

        <?php if($imgPool): $firstImg = array_shift($imgPool); ?>
            <div class="headerimg first" style="background-image:url(<?php echo $firstImg['src']; ?>)"></div>
            <?php if(count($imgPool) > 1): //If more than one header, lazy loading for rest  ?>
                <script>
                    let extraHeaders = <?php echo json_encode(array_map(function($e) {
                        return $e['src'];
                    }, $imgPool)); ?>;
                </script>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</header>