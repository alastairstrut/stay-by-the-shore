<?php

function load_acf_header_assets(){
    wp_enqueue_style('acf-header-base-css', get_template_directory_uri() . '/includes/acf-headers/css/style.min.css', array(), '1.0.0', 'all');
    wp_enqueue_script('acf-header-js', get_template_directory_uri() . '/includes/acf-headers/js/headers.min.js', array('jquery'), '0.0.0.1', true);
}
add_action('wp_enqueue_scripts', 'load_acf_header_assets');
