<?php
    $img = '';
    $link = '';
    $link = apply_filters('the_permalink', get_permalink());
    $excerpt = apply_filters('the_excerpt',get_the_excerpt());
    $thedate = get_the_date('d M Y');

    if(has_post_thumbnail()){
        $img = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'large');
        $img = $img[0];
        $img = '<a href="'.$link.'"><img class="blog-image" src="'. $img .'" alt="' . get_the_title() .'"></a>';
    }else{
        $img = '<a href="'.$link.'"><img class="blog-image" src="'. get_bloginfo('template_url') .'/assets/placeholder.jpg" alt="' . get_the_title() .'"></a>';
    }

    echo '<div class="row blog-item">
    <div class="col-lg-5 blog-image">
    ' . $img . '
    </div>
    <div class="col-lg-7">
        <div class="blog-box">
            <a href="'.$link.'"><h5>' . the_title('','',false). '</h5></a>
            <small>' . $thedate . '</small>
            ' . $excerpt . '
        </div>
    </div>
</div>';
?>