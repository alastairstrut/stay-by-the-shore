<?php

// Admin Page

function theme_create_page(){
    require_once(get_template_directory() . '/includes/templates/admin.php');
}

function custom_settings(){
    register_setting('settings-group', 'first_name');
    register_setting('settings-group', 'last_name');
    register_setting('settings-group', 'user_description');
}

function theme_settings_page(){
    echo '<h1>Admin Css Page</h1>';
}

// -----