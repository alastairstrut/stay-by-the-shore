<?php

    define('STRUT_CACHE_BURST', '0.0.7');

    class StrutAssetInliner{

        function __construct(){
            add_action('wp_enqueue_scripts', array(&$this, 'addScriptsAndStylesHere') );
            add_action('init', array(&$this, 'init'));
            add_action('wp_head',array(&$this, 'outputCriticalCSS'));
            add_action('wp_footer',array(&$this, 'outputCriticalJS'));
            add_action('wp_footer', array(&$this, 'outputDeferredCSS'));
            add_action('wp_footer', array(&$this, 'outputDeferredJS'));
            add_action('wp_print_scripts', array(&$this, 'sortGlobalWPJSQueue'));
            add_action('wp_print_styles', array(&$this, 'sortGlobalWPCSSQueue'));
        }


        function init(){

            //For all dependancies, remove text/javascript and defer
            add_filter( 'script_loader_tag', function($tag, $handle = null , $src = null){
                if(!strpos($_SERVER["REQUEST_URI"],"wp-admin")){

                    //Strip out text/javascript nonsense
                    $tag = preg_replace('/\stype=(\'|")text\/javascript(\'|")/', '', $tag);

                    //If script has a src but is not defered, do so to avoid jQuery conflicts
                    if(preg_match('/script.*?src=/', $tag) && !preg_match('/\sdefer/', $tag)){
                        $tag = preg_replace('/<script\s/', '<script defer ', $tag);
                    }

                     // !!! PREVENT A SECOND COPY OF JQUERY FROM BEING LOADED BY A 3rd-PARTY PLUGIN !!!
                     if(preg_match('#wp-includes/js/jquery/jquery.js#', $tag)){
                        return '';
                    }
                }

                return $tag;
            });

            // all actions related to emojis
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
            remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
            remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
            // Remove the REST API endpoint.
            // remove_action('rest_api_init', 'wp_oembed_register_route');
            // Turn off oEmbed auto discovery.
            // Don't filter oEmbed results.
            // remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
            // Remove oEmbed discovery links.
            // remove_action('wp_head', 'wp_oembed_add_discovery_links');
            // Remove oEmbed-specific JavaScript from the front-end and back-end.
            // remove_action('wp_head', 'wp_oembed_add_host_js');
        }

        function sortGlobalWPCSSQueue() {
            if(!strpos($_SERVER["REQUEST_URI"],"wp-admin")){
                global $wp_styles;
                global $headerCSS;
                if(!is_array($headerCSS))
                    $headerCSS = array();

                foreach( $wp_styles->queue as $handle ) :

                    if($handle != 'theme-google-fonts' && isset($wp_styles->registered[$handle])){
                        wp_dequeue_style($handle);

                        foreach ($wp_styles->registered[$handle]->deps as $depv) {
                            if(!isset($headerCSS[$depv]) && $wp_styles->registered[$depv]->src!=''){
                                $headerCSS[$depv] = $wp_styles->registered[$depv]->src;
                                wp_dequeue_style($depv);
                            }
                        }

                        if(!isset($headerCSS[$handle]) && $wp_styles->registered[$handle]->src!=''){
                            $headerCSS[$handle] = $wp_styles->registered[$handle]->src;
                            wp_dequeue_style($handle);
                        }
                    }
                endforeach;
            }
        }


        function sortGlobalWPJSQueue() {

            if(preg_match('#wp-admin#',$_SERVER['REQUEST_URI'])){
                return true;
            }
            //queue all scripts and put them in an object we can access and later on use in footer
            global $wp_scripts;
            global $footerDefferedScripts;


            if(!is_array($footerDefferedScripts)){
                $footerDefferedScripts = array();
            }



            if(!is_null($wp_scripts)){
                foreach( $wp_scripts->queue as $handle ){

                    if(isset($wp_scripts->registered[$handle])){

                        $scriptSrc = $wp_scripts->registered[$handle]->src;

                        if($scriptSrc != '' && !in_array($scriptSrc, $footerDefferedScripts)){
                            $footerDefferedScripts[$handle] = $wp_scripts->registered[$handle];
                        }
                        wp_dequeue_script($handle);
                    }
                }
            }

        }


        function outputCriticalJS(){
            global $post;
            global $footerDefferedScripts;


            if( (is_array($footerDefferedScripts) && count($footerDefferedScripts) > 0) ){

                $critCount = 0;
                foreach($footerDefferedScripts as $s){
                    if(preg_match('#critical-.*#', $s->handle)){
                        ++$critCount;
                    }
                }

                //Make sure we've got non critical assets to output
                if($critCount > 0){
                    echo "<!-- Strut Critical JS -->\r\n";

                    $srcList = array();

                    //ignore ssl verification
                    $arrContextOptions=array(
                        "ssl"=>array(
                            "verify_peer"=>false,
                            "verify_peer_name"=>false,
                        ),
                    );

                    foreach($footerDefferedScripts as $s){
                        $enqSrc = trim($s->src);

                        if(preg_match('#critical-.*#', $s->handle)){
                            //If WP script we need to append blogurlu
                            if(preg_match('#^\/wp-includes\/#', $enqSrc)){
                                $enqSrc = get_bloginfo('wpurl') . $enqSrc;
                            }





                            echo "<script>\r\n";

                            if(preg_match('#wp\-includes#', $enqSrc)){
                                $tmpItm = file_get_contents(get_bloginfo('wpurl') . $enqSrc, false, stream_context_create($arrContextOptions));
                            }else if(preg_match('#^' . get_bloginfo('template_url') . '#', $enqSrc)){

                                 //Grab file URL instead of remote URL
                                $fileURL = str_replace(get_bloginfo('template_url'), get_template_directory(), $enqSrc);
                                $tmpItm = file_get_contents($fileURL, false, stream_context_create($arrContextOptions));
                            }else{
                                $tmpItm = file_get_contents($enqSrc, false, stream_context_create($arrContextOptions));
                            }

                            echo $tmpItm . "\r\n";
                            echo "</script>\r\n";

                            // //If scripts are localized, include the extras for them
                            if( isset($s->extra) && isset($s->extra['data']) ) {
                                echo "<script>\r\n{$s->extra['data']}\r\n</script>\r\n\r\n";
                            }
                        }
                    }
                }
            }

        }


        function outputDeferredJS(){
            global $post;
            global $footerDefferedScripts;

            if( (is_array($footerDefferedScripts) && count($footerDefferedScripts) > 0) ){
                $srcList = array();

                //Make sure we've got non critical assets to output
                $nonCritCount = 0;
                foreach($footerDefferedScripts as $s){
                    if(!preg_match('#critical-.*#', $s->handle)){
                        ++$nonCritCount;
                    }
                }

                if($nonCritCount > 0){


                    //If scripts have after effects, include them
                    foreach($footerDefferedScripts as $s){
                        if( isset($s->extra) && isset($s->extra['before']) ) {
                            foreach($s->extra['before'] as $as){
                                if(!empty($as)){
                                    echo "<script>\r\n {$as} \r\n</script>\r\n";
                                }
                            }
                        }
                    }


                    //ignore ssl verification
                    $arrContextOptions=array(
                        "ssl"=>array(
                            "verify_peer"=>false,
                            "verify_peer_name"=>false,
                        ),
                    );

                    foreach($footerDefferedScripts as $s){
                        $enqSrc = trim($s->src);

                        if(!preg_match('#critical-.*#', $s->handle)){
                            //If WP script we need to append blogurlu
                            if(preg_match('#^\/wp-includes\/#', $enqSrc)){
                                $enqSrc = get_bloginfo('wpurl') . $enqSrc;
                            }

                            //Ensure cache burst on theme assets
                            if(preg_match('#^' . get_bloginfo('template_url') . '#', $enqSrc)){
                                $enqSrc = $enqSrc . "?v=" . STRUT_CACHE_BURST;
                            }
                            $srcList[] = $enqSrc;

                            //If scripts are from plugins, include any extras for them
                            if( isset($s->extra) && isset($s->extra['data']) ) {
                                echo "<script>\r\n{$s->extra['data']}\r\n</script>\r\n";
                            }
                        }
                    }

                    //Ensure jquery is front and center!
                    $jQuerySrcIndex = -1;
                    for($i = 0; $i< count($srcList); $i++){
                        /**
                         * If we have what we think is jquery, take a copy of the src
                         * remove it from the array and add it into the front of
                         * the array after the loop
                         */
                        if(preg_match('#jquery(\.min)?\.js#', $srcList[$i])){
                            //Keep track of the index
                            $jQuerySrcIndex = $i;
                        }
                    }

                    if($jQuerySrcIndex != -1){
                        $jqSrc = $srcList[$jQuerySrcIndex];
                        unset($srcList[$jQuerySrcIndex]);
                        // array_unshift($srcList, $jqSrc);
                        echo '<script defer src="'.$jqSrc.'"></script>';
                    }



                    ?>

                        <script>
                            var scriptsToLoad = ['<?php echo implode("','", $srcList); ?>'];
                            var scriptsToLoadIndex = 0;

                            function ht_scripts_to_load(){
                                if(scriptsToLoad[scriptsToLoadIndex]){
                                    (function(d) {
                                        var wf = d.createElement('script'); //var s = d.scripts[0];
                                        wf.defer = true;
                                        wf.src = scriptsToLoad[scriptsToLoadIndex];
                                        wf.onload = ht_scripts_to_load;
                                        scriptsToLoadIndex++;
                                        document.body.appendChild(wf);
                                    })(document);
                                }
                            }
                            ht_scripts_to_load();
                        </script>
                    <?php
                    //If scripts have after effects, include them
                    foreach($footerDefferedScripts as $s){
                        $srcList[] = $s->src;

                        if( isset($s->extra) && isset($s->extra['after']) ) {
                            foreach($s->extra['after'] as $as){
                                if(!empty($as)){
                                    echo "<script defer>\ndocument.addEventListener(\"DomContentLoaded\", function(e){ {$as} });\r\n</script>\r\n";
                                }
                            }
                        }
                    }
                }
            }
        }

        //Inlines CSS straight into the <head> tag
        function outputCriticalCSS(){
            global $headerCSS;
            if(is_array($headerCSS) && count($headerCSS)>0){

                //Make sure we've got critical assets to output
                $critCount = 0;
                foreach ($headerCSS as $cssHandle => $srcURL) {
                    if(preg_match('#critical-.*#', $cssHandle)){
                        ++$critCount;
                    }
                }

                if($critCount > 0){
                    echo "<!-- Strut Critical CSS -->\r\n";
                    echo "<style>\r\n";


                            //ignore ssl verification
                            $arrContextOptions=array(
                                "ssl"=>array(
                                    "verify_peer"=>false,
                                    "verify_peer_name"=>false,
                                ),
                            );

                            foreach ($headerCSS as $cssHandle => $srcURL) {
                                if(preg_match('#critical-.*#', $cssHandle)){
                                    if(preg_match('#wp\-includes#', $srcURL)){
                                        $tmpItm = file_get_contents(get_bloginfo('wpurl') . $srcURL, false, stream_context_create($arrContextOptions));
                                    }else{

                                        //Ensure cache burst on theme assets
                                        if(preg_match('#^' . get_bloginfo('template_url') . '#', $srcURL)){
                                            //Grab file URL instead of remote URL
                                            $fileURL = str_replace(get_bloginfo('template_url'), get_template_directory(), $srcURL);
                                            $tmpItm = file_get_contents($fileURL, false, stream_context_create($arrContextOptions));
                                        }
                                    }
                                    $filename = explode("/", $srcURL);
                                    $filename = $filename[count($filename)-1];

                                    echo "\r\n\r\n/** {$srcURL} **/ \r\n\r\n";

                                    $url = str_replace($filename,"",$srcURL);


                                    /**
                                     * Change all local url() values to fully qualified ones e.g:
                                     *   - 'img/something.png' => www.site.com/wp-config/themes/something/img/something.png
                                     *
                                     * Anything starting with 'http' or 'data' is left out as either exteneral resource
                                     * or a Data URI.
                                     *
                                     */
                                    $tmpItm = preg_replace_callback(
                                        '#url\((.*?)\)#',
                                        function($match) use($url){
                                            $orig = $match[0];

                                            $strippedURL = preg_replace("#url\((\\\"|\')(.*?)(\\\"|\')\)#","url($2)",$orig);

                                            if(preg_match('#(https?|data:)#', $strippedURL)){
                                                return $orig;
                                            }else{
                                                $urlPrefixedWithTemplateURL = preg_replace("#url\((.*?)\)#","url({$url}$1)",$strippedURL);
                                                return $urlPrefixedWithTemplateURL;
                                            }
                                        },
                                        $tmpItm
                                    );

                                    echo $tmpItm ."\r\n\r\n";
                                }

                            }


                    echo "</style>\r\n\r\n";
                }
            }
        }

        // "Lazy loads" styles at bottom of the page
        function outputDeferredCSS(){
            global $headerCSS;
            if(is_array($headerCSS) && count($headerCSS)>0){

                //Make sure we've got non critical assets to output
                $nonCritCount = 0;
                foreach ($headerCSS as $cssHandle => $headerCSSItm) {
                    if(!preg_match('#critical-.*#', $cssHandle)){
                        ++$nonCritCount;
                    }
                }

                if($nonCritCount > 0){

                    echo "\n<noscript id=\"deferred-styles\">\n\n";

                    foreach ($headerCSS as $cssHandle => $headerCSSItm) {
                        if(!preg_match('#critical-.*#', $cssHandle)){
                            $filename = explode("/", $headerCSSItm);
                            $filename = $filename[count($filename)-1];


                            if(preg_match('#wp\-includes#', $headerCSSItm)){
                                echo '<link rel="stylesheet" href="'. get_bloginfo('wpurl') . $headerCSSItm.'"/>' . "\n";
                            }else{

                                //Ensure cache burst on theme assets
                                if(preg_match('#^' . get_bloginfo('template_url') . '#', $headerCSSItm)){
                                    $headerCSSItm = $headerCSSItm . '?v=' . STRUT_CACHE_BURST;
                                }

                                echo '<link rel="stylesheet" href="'. $headerCSSItm .'"/>' . "\n";
                            }
                        }
                    }
                    echo "</noscript>\n\n";

                    ?>
                    <script>
                        var loadDeferredStyles = function() {
                            var addStylesNode = document.getElementById("deferred-styles");
                            var replacement = document.createElement("div");
                            replacement.innerHTML = addStylesNode.textContent;
                            document.body.appendChild(replacement)
                            addStylesNode.parentElement.removeChild(addStylesNode);
                        };
                        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
                        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
                        else window.addEventListener('load', loadDeferredStyles);
                    </script>
                    <?php
                }
            }
        }

        //Wrapper for wp_enqueue_style
        function addCSS($url){
            $handle = '';
            if(!empty($url)){
                $url = $this->translateShortURLtoFullURL($url);

                wp_enqueue_style(
                    $handle = sha1($url),
                    $src =  $url
                );
            }
        }

        //Wrapper for wp_enqueue_style
        function addCriticalCSS($url){
            $handle = '';
            if(!empty($url)){
                $url = $this->translateShortURLtoFullURL($url);

                wp_enqueue_style(
                    $handle = 'critical-' . sha1($url),
                    $src =  $url
                );
            }
        }

        //Wrapper for wp_enqueue_script
        function addJS($url = ''){
            $handle = '';
            if(!empty($url)){
                $url = $this->translateShortURLtoFullURL($url);

                wp_enqueue_script(
                    $handle = sha1($url),
                    $src =  $url,
                    $deps = array(),
                    $ver = null,
                    $in_footer = false
                );
            }
        }

        //Wrapper for wp_enqueue_script (But puts into <head>)
        function addCriticalJS($url = ''){
            $handle = '';
            if(!empty($url)){
                $url = $this->translateShortURLtoFullURL($url);

                wp_enqueue_script(
                    $handle = 'critical-' . sha1($url),
                    $src =  $url,
                    $deps = array(),
                    $ver = null,
                    $in_footer = false
                );
            }
        }

        /**
         * a url of '/js/script.js/' -> $templateURL . '/js/script/js'
         * Remote URLS are left as-is
         **/
        function translateShortURLtoFullURL($url = ''){
            if(!empty($url)){
                //If we are dealing with a rel file
                if(substr($url, 0, 1) === '/'){
                    $url = get_bloginfo('template_url') . $url;
                }
            }

            return $url;
        }

        /**
         * Add your theme script/styles in this block below
         *
         * use $this->addJS('/path/to/file') or $this->addCSS('/path/to/file');
         *
         * NOTE: path to file is from the theme root, so /js/myscript.js automatically equates to /path/to/my/themefolder/js/myscript.js
         *
         * For 3rd party scripts/styles, like google fonts, use the full http(s):// url
         * e.g $this->addCSS('https://fonts.googleapis.com/css?family=Lato:400,600')
         **/
        function addScriptsAndStylesHere(){
            global $post;

            //LEAVE THIS HERE - REMOVING AND ADDING ADDS IT TO THE FRONT OF THE QUEUE
            wp_dequeue_script('jquery-core');
            wp_enqueue_script('jquery-core');

            /**
             * Examples
             * --------
             *
             * In these examples, Local means in your theme, Remote means served from the internet.
             *
             * Local JavaScript file:  $this->addJS('/js/myscript.min.js');
             * Remote JavaScript file: $this->addJS('https://cdnjs.cloudflare.com/ajax/libs/SuperScrollorama/1.0.3/jquery.superscrollorama.js');
             *
             * Local CSS file:  $this->addJS('/style.css');
             * Remote CSS file: $this->addCSS('https://fonts.googleapis.com/css?family=Lato:400,600')
             *
             *
             * If you need an asset to be inlined in the head, such as an ATF.css or
             * critical.js file that NEEDS to be there use the following variants for
             * CSS and JS inclustion
             *
             * $this->addCriticalCSS('/css/atf.css');
             * $this->addCriticalJS('/js/critical.css');
             *
             *
             * If you, lets say have a stylesheet just for the front page you want to do:
             *
             * if(is_front_page()){
             *     $this->addCSS('/css/front.min.css');
             * }
             *
             * If you have a script that only gets run on the page with the slug 'contact':
             *
             * if(is_page('contact')){
             *     $this->addJS('/js/contact-form-extras.min.js');
             * }
             *
             * You can replace 'is_front_page()' or 'is_page()' with a whole bunch of other things:
             * - is_page('contact-us') will only do this on a page with the slug 'contact-us'
             * - is_page(32) will only do this on the page with the ID 32
             * - is_single() will put it on blog post pages
             * - is_home() will put it on the main blog listing page
             * - is_archive() will put it on the blog post archives
             * - is_category() will put it on the blog post category listings page
             *
             **/

            /** START ADDING SCRIPTS/STYLES HERE **/


            //Critical CSS - Inlined in <head> tag
            $this->addCriticalCSS('/css/bootstrap' . (WP_DEBUG ? '' : '.min') . '.css');

            //Non Critical CSS - Loaded after main page load
            $this->addCSS('https://fonts.googleapis.com/css?family=Charm:400,700%7CMontserrat:400,400i,700&display=swap');

            $this->addCSS('/style' . (WP_DEBUG ? '' : '.min') . '.css');

            $this->addCSS('/css/baguetteBox' . (WP_DEBUG ? '' : '.min') . '.css');

            function load_css_javascript_function(){
                wp_enqueue_style('customstyle', get_template_directory_uri() . '/style.min.css', array(), '1.0.0', 'all');
                wp_enqueue_script('customjs', get_template_directory_uri() . '/js/general.js', array(), '1.0.0', true);
            }

            // action - telling wordpress when to execute functions

            add_action('wp_enqueue_scripts', 'load_css_javascript_function');



            //Critical JS - script inlined in <head> tag


            //Non Critical JS - Added with defer attribute at the bottom of the page
            $this->addJS('/js/intersection-observer' . (WP_DEBUG ? '' : '.min') . '.js');
            $this->addJS('/js/lazeee-load' . (WP_DEBUG ? '' : '.min') . '.js');


            $this->addJS('/js/bootstrap' . (WP_DEBUG ? '' : '.min') . '.js');

            //Cta slider - relies on hammer.js for touch events
            $this->addJS('/js/baguetteBox' . (WP_DEBUG ? '' : '.min') . '.js');
            $this->addJS('/js/general' . (WP_DEBUG ? '' : '.min') . '.js');
        }
    }


    $strutAssetInliner = new StrutAssetInliner();
