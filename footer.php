<footer>

<div class="container">
    <div class="row">
        <div class="col-lg-4">
            <a href="<?php bloginfo('template_url'); ?>/home/">
                <img id="footer-logo" src="<?php bloginfo('template_url'); ?>/assets/footer-logo.svg" alt="logo">
            </a>
        </div>
        <div class="col-lg-4">
            <h4>Get in Touch</h4>
            <p>Rob & Emma McKay,</p>
            <p>The Old School House,</p>
            <p>Strath,</p>
            <p>Gairloch,</p>
            <p>IV21 2BZ</p>
            <hr class="d-none d-lg-block">
                <div style="margin: 10px 0;">
                    <p><img class="footer-icons" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/phone-icon.svg" alt="phone">
                    <a href="tel:+4401445712814">01445 712814</a><span style="margin: 0 10px;">/</span><a href="tel:+4407739709790">07739709790</a></p>
                </div>
                <div style="margin: 10px 0;">
                    <p><img class="footer-icons" src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" style="margin-top: 4px;" data-src="<?php bloginfo('template_url'); ?>/assets/mail-icon.svg" alt="mail">
                    <a href="mailto:staybytheshore@gmail.com">staybytheshore@gmail.com</a></p>
                </div>
                <!-- Uncomment this section when Facebook and Instagram accounts are created for Stay by the Shore
                    <div style="margin: 30px 0 0px;">
                    <a href="#">
                        <img src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/insta-icon.svg" style="margin-right: 30px;" alt="instagram">
                    </a>
                    <a href="#">
                        <img src="<?php bloginfo('template_url'); ?>/assets/placeholder.png" data-src="<?php bloginfo('template_url'); ?>/assets/facebook-icon.svg" alt="facebook">
                    </a>
                </div> -->
        </div>
        <div class="col-lg-4 d-none d-lg-block">
            <h4>Quick Links</h4>
                <div class="row">
                    <ul>
                        <li><a href="<?php bloginfo('url'); ?>/the-cottage/">The Cottage</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/the-bb/">B&B</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/booking/">Booking</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/about-us/">About Us</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/see-do/">See and Do</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php bloginfo('url'); ?>/find-us/">Find Us</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/blog/">Blog</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/terms-conditions/">Terms & Conditions</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/privacy-policy/">Cookies & Privacy</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/site-credits/">Site Credits</a></li>
                    </ul>
                </div>
        </div>
        <div id="useful-links" class="d-lg-none">
            <h4>Useful Links</h4>
            <p><a href="<?php bloginfo('url'); ?>/find-us/">Find Us</a></p>
            <p><a href="<?php bloginfo('url'); ?>/terms-conditions/">Terms & Conditions</a></p>
            <p><a href="<?php bloginfo('url'); ?>/privacy-policy/">Cookies & Privacy</a></p>
            <p><a href="<?php bloginfo('url'); ?>/site-credits/">Site Credits</a></p>
        </div>
    </div>
</div>

</footer>

<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>© The Old School House B&B and Beul Na Mara.</p>
            </div>
        </div>
    </div>
</div>

    <?php wp_footer(); ?>
</body>
</html>