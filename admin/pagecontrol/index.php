<?php
    if ( ! defined( 'ABSPATH' ) ) exit('no access'); // disable direct access

    add_action('admin_menu', 'ht_pagecontrol');
    function ht_pagecontrol(){
        add_pages_page('Page Control', 'Page Control', 'manage_options', 'ht_pagecontrol_page', 'ht_pagecontrol_page');
    }

    add_action( 'init', 'ht_pagecontrol_init' );
    function ht_pagecontrol_init(){
        //call filter to remove pages
        add_filter('get_pages','ht_exclude_pages');
    }

    function ht_exclude_pages($pages){
        //only do this on front end not backend
        if(!strpos($_SERVER["REQUEST_URI"],"wp-admin")){
            $excludedPages = get_option('_ht_exclude_pages','');
            if($excludedPages==''){
                $excludedPages = array();
            }else{
                if(preg_match('#,#', $excludedPages)){
                    $excludedPages = explode(",", $excludedPages);
                }else{
                    if(is_numeric($excludedPages))
                        $excludedPages = array($excludedPages);
                }
            }

            foreach ($pages as $page) {
                $exclude = false;
                //check if page is in the exclude array
                if(in_array($page->ID, $excludedPages)){
                    $exclude = true;
                }

                //check if page ancestors are in the excluded array
                if(!$exclude){
                    $anscetors = get_post_ancestors($page->ID);
                    if($anscetors && is_array($anscetors) && count($anscetors)>0){
                        foreach ($anscetors as $anscetor) {
                            if(in_array($anscetor, $excludedPages)){
                                $exclude = true;
                            }
                        }
                    }
                    unset($anscetors);
                }

                if($exclude){
                    $pageLoc = ht_get_page_location($page->ID, $pages);
                    unset($pages[$pageLoc]);
                }
            }
        }
        return $pages;
    }

    function ht_get_page_location($pageId, $pages){
        foreach($pages as $pagek => $page){
            if($page->ID == $pageId){
                return $pagek;
            }
        }
    }

    function ht_pagecontrol_page(){
        wp_enqueue_script('jquery-ui-sortable');
        $excludedPages = get_option('_ht_exclude_pages','');
        if($excludedPages==''){
            $excludedPages = array();
        }else{
            if(preg_match('#,#', $excludedPages)){
                $excludedPages = explode(",", $excludedPages);
            }else{
                if(is_numeric($excludedPages))
                    $excludedPages = array($excludedPages);
            }
        }

        //get submitted data
        if(isset($_GET['submit'])){
            $includedPages = (isset($_POST['ht_page_chks']) && is_array($_POST['ht_page_chks']) ? $_POST['ht_page_chks'] : array());

            //now loop through all pages to make sure the excludedpages are crteaed
            $excludedPages = array();
            $excPages = get_pages();
            foreach ($excPages as $excPage) {
                if(!in_array($excPage->ID, $includedPages)){
                    $excludedPages[] = $excPage->ID;
                }
            }

            update_option('_ht_exclude_pages', implode(",", $excludedPages));

            //update order
            $counter = 1;
            foreach ($_POST['ht_order'] as $orderk => $orderv) {
                wp_update_post(array(
                    'ID'            => $orderv,
                    'menu_order'    => $counter,
                ));
                $counter++;
            }
        }

        ?>
        <style type="text/css">
            .wrap ul ul{padding-left: 30px;}
            .ui-state-highlight{display: block; width: 50%; height: 20px; border: 1px dashed #cccccc; background-color: #e1e1e1;}
            .wrap a.hoverlink{display: none;}
            .wrap ul > li:hover > a.hoverlink{display: inline-block;}
        </style>
        <div class="wrap">
            <h2>Page Control</h2>
            <p>Un-tick boxes for pages you want to exclude from navigation, drag and drop pages to re-order them and click the "Save Changes" button to save your changes.</p>
            <?php

                $pageArgs = array();
                $pageArgs['sort_column'] = 'menu_order';
                $pagesMain = get_pages($pageArgs);
                $pagesArray = array();

                foreach ($pagesMain as $page) {
                    $tmpItm = new stdClass();
                    $tmpItm->ID = $page->ID;
                    $tmpItm->Title = $page->post_title;
                    $tmpItm->Status = $page->post_status;
                    $tmpItm->Include = (!in_array($page->ID, $excludedPages) ? 1: 0);

                    if($page->post_parent==0){
                        $pagesArray[$page->ID] = $tmpItm;
                    }else{
                        $parents = array_reverse(get_post_ancestors($page->ID));
                        //five level navigation

                        switch(count($parents)){
                            case 1:
                                $obj = &$pagesArray[$parents[0]]->Children;
                                break;
                            case 2:
                                $obj = &$pagesArray[$parents[0]]->Children[$parents[1]]->Children;
                                break;
                            case 3:
                                $obj = &$pagesArray[$parents[0]]->Children[$parents[1]]->Children[$parents[2]]->Children;
                                break;
                            case 4:
                                $obj = &$pagesArray[$parents[0]]->Children[$parents[1]]->Children[$parents[2]]->Children[$parents[3]]->Children;
                                break;
                            case 5:
                                $obj = &$pagesArray[$parents[0]]->Children[$parents[1]]->Children[$parents[2]]->Children[$parents[3]]->Children[$parents[4]]->Children;
                                break;
                        }

                        if(!isset($obj))
                            $obj = array();
                        $obj[$page->ID] = $tmpItm;
                    }
                }

                //clear all keys from array
                function ht_clearArrays(&$tmpItm){
                    if(isset($tmpItm) && is_array($tmpItm)){
                        $tmpItm = array_values($tmpItm);
                        foreach ($tmpItm as $tmpItm2) {
                            if(isset($tmpItm2->Children) && is_array($tmpItm2->Children))
                                ht_clearArrays($tmpItm2->Children);
                        }
                    }
                }
                $pagesArray = array_values($pagesArray);
                foreach ($pagesArray as $arrItm) {
                    if(isset($arrItm->Children) && is_array($arrItm->Children))
                        ht_clearArrays($arrItm->Children);
                }


            ?>
            <form id="ht_pages_form" name="ht_pages_form" action="edit.php?post_type=page&amp;page=ht_pagecontrol_page&amp;submit=1" method="post">
                <div id="ht_page_divs"></div>
                <input type="submit" value="Save Changes" class="button button-primary" />
            </form>
        </div>
        <script>
            <!--
            var ht_pages = <?php echo json_encode($pagesArray); ?>;

            jQuery(document).ready(function(){
                ht_reDrawPages();

                jQuery('#ht_page_divs > ul').sortable({
                    placeholder: "ui-state-highlight"
                });
                jQuery('#ht_page_divs > ul ul').sortable({
                    placeholder: "ui-state-highlight"
                });
            });

            function ht_reDrawPages(){
                jQuery('#ht_page_divs').html('<ul></ul>');
                for(page in ht_pages){
                    var txtTmp = ht_paintPage(ht_pages[page]);
                    jQuery('#ht_page_divs > ul').append(txtTmp);
                }
            }

            function ht_paintPage(page){
                if(page.Children){
                    var tmpTxt = '<li><label><input type="hidden" name="ht_order[]" value="'+ page.ID +'" /><input '+ (page.Include==1 ? ' checked="checked" ' : '') +' type="checkbox" class="ht_page_chk_class" name="ht_page_chks[]" value="' + page.ID + '" /> ' + page.Title + '</label>&nbsp;&nbsp;&nbsp;<a class="hoverlink" href="post.php?post='+ page.ID +'&action=edit">Edit</a><ul>';
                    for(subpage in page.Children){
                        tmpTxt += ht_paintPage(page.Children[subpage]);
                    }
                    tmpTxt += '</ul></li>';
                }else{
                    var tmpTxt = '<li><label><input type="hidden" name="ht_order[]" value="'+ page.ID +'" /><input '+ (page.Include==1 ? ' checked="checked" ' : '') +' type="checkbox" class="ht_page_chk_class" name="ht_page_chks[]" value="' + page.ID + '" /> ' + page.Title + '</label>&nbsp;&nbsp;&nbsp;<a class="hoverlink" href="post.php?post='+ page.ID +'&action=edit">Edit</a></li>';
                }
                return tmpTxt;
            }
            //-->
        </script>
        <?php
    }
?>
