<?php

//add page control
include_once(get_template_directory() . '/admin/pagecontrol/index.php');
include_once(get_template_directory() . '/includes/acf-headers/index.php');
include_once(get_template_directory() . '/includes/class-strut-asset-inliner.php');

// Allows for more features in wordpress

add_theme_support('post-formats');
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');

function destroy_emojis(){
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    add_image_size( 'header-size', 1600 );
}
add_action('init', 'destroy_emojis');

// Dictates the length of excerpts

function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// Removes the wordpress version from meta tags which increases site security

function remove_meta_version(){
    return '';
}

add_filter('the_generator', 'remove_meta_version');

// Adds a class to buttons added by the post_link functions

function posts_link_attributes() {
    return 'class="button-c"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

// -----

require get_template_directory() . '/includes/function-admin.php';


//Strip out type="text/javascript" nonsense
add_filter( 'script_loader_tag', function($tag, $handle = null , $src = null){
    if(!strpos($_SERVER["REQUEST_URI"],"wp-admin")){
        $tag = preg_replace('/\stype=(\'|")text\/javascript(\'|")/', '', $tag);
    }
    return $tag;
});